package vita.sanus.caretaker;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

/**
 * Created by ankit on 13/4/15.
 */
public class AudioIn implements Runnable {
    private Thread worker;
    private AudioRecord audioInput;
    private volatile boolean isRecording = false;
    
    private int inputSource = MediaRecorder.AudioSource.MIC;
    private int sampleSize = 44100;
    private int channel_config = AudioFormat.CHANNEL_IN_MONO;
    private int format = AudioFormat.ENCODING_PCM_16BIT;
    private int bufferSize = AudioRecord.getMinBufferSize(sampleSize, channel_config, format);

    public short[] audioBuffer = new short[bufferSize];

    public AudioIn(String threadName) {
        worker = new Thread(this, threadName);
        worker.start();
    }

    private synchronized void startRecording() {

        audioInput = new AudioRecord(inputSource, sampleSize, channel_config, format, bufferSize);


        System.out.println("Thread begin******************************");


        if (audioInput.getState() == AudioRecord.STATE_INITIALIZED) {
            audioInput.startRecording();
            isRecording = true;

            Thread processingThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    processAudioIn();
                }
            }, "AudioIn.processingThread");

            processingThread.start();

//            audioInput.read(audioBuffer, 0, bufferSize);
        };
    }

    public void processAudioIn() {
        while (isRecording) {
            int read = audioInput.read(audioBuffer, 0, bufferSize);
            System.out.println(audioBuffer[0]);
//            System.out.println("Recording begin******************************   " + audioInput.getRecordingState());
        }
    }

    public synchronized void stopRecording() {
        if (audioInput.getState() == AudioRecord.STATE_INITIALIZED) {
            System.out.println("Recording is still on******************************   " + audioInput.getRecordingState());
            isRecording = false;
            audioInput.stop();
            audioInput.release();
            System.out.println("Recording stop******************************   " + audioInput.getRecordingState());
        }
    }

    @Override
    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        this.startRecording();
    }


}
